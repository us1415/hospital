from django.test import LiveServerTestCase
from attendance.models import Appointment

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait


class BaseLiveTest(LiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        cls.driver= webdriver.Firefox()
        super(BaseLiveTest, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        super(BaseLiveTest, cls).tearDownClass()
        cls.driver.quit()

class AppointmentFormTest(BaseLiveTest):
    def test_form(self):
        self.driver.get('http://127.0.0.1:8000/schedule/')
        # Populate form
        self.driver.find_element_by_xpath('//select[@name="doctor"]/option[@value="2"]').click()
        self.driver.find_element_by_name('date').send_keys('2016-08-17')
        self.driver.find_element_by_name('time').send_keys('08:00:00')
        self.driver.find_element_by_name('surname').send_keys('test')
        self.driver.find_element_by_name('forename').send_keys('test')
        
        self.driver.find_element_by_xpath('//input[@value="Apply"]').click()

        WebDriverWait(self.driver, 5).until(
            lambda x: self.driver.find_element_by_xpath('//div[@class="alert alert-success"]'))

    def test_duplicate(self):
        self.driver.get('http://127.0.0.1:8000/schedule/')
        # Populate form
        self.driver.find_element_by_xpath('//select[@name="doctor"]/option[@value="2"]').click()
        self.driver.find_element_by_name('date').send_keys('2016-08-17')
        self.driver.find_element_by_name('time').send_keys('08:00:00')
        self.driver.find_element_by_name('surname').send_keys('test')
        self.driver.find_element_by_name('forename').send_keys('test')
        self.driver.find_element_by_xpath('//input[@value="Apply"]').click()

        WebDriverWait(self.driver, 5).until(
            lambda x: self.driver.find_element_by_xpath('//h3[@class="text-danger"]'))


