from django.db import models
from django.core.urlresolvers import reverse


class Doctor(models.Model):
    forename = models.CharField(max_length=255)
    surname = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255)
    area = models.CharField(max_length=255)

    def __str__(self):
        return "%s %s"%(self.forename, self.surname)

    def get_absolute_url(self):
        return reverse('doctor', args=[str(self.slug)])
