from django.contrib import admin
from doctors.models import Doctor

@admin.register(Doctor)
class DoctorAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('surname',)}
    list_display = ['forename', 'surname', 'area']
    list_filter = ['area']