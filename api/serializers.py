from doctors.models import Doctor
from attendance.models import Appointment
from rest_framework import serializers

class DoctorSerializer(serializers.HyperlinkedModelSerializer):
    appointments = serializers.StringRelatedField(many=True)
    class Meta:
        model = Doctor
        fields = ('forename', 'surname', 'slug', 'area', 'appointments')
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }

class AppointmentSerializer(serializers.ModelSerializer):
    class Meta:
    	model = Appointment
    	fields = ('status', 'doctor', 'forename', 'surname', 'time', 'date')