from django.conf.urls import include, url
from api.views import DoctorViewSet, AppointmentCreateView
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'doctors', DoctorViewSet, base_name='doctors')

urlpatterns = [
	url(r'^appointments/$', AppointmentCreateView.as_view()),
]

urlpatterns += router.urls