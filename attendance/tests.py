import random
import time
import datetime

from django.test import TestCase, TransactionTestCase
from django.db import IntegrityError
from django.core.urlresolvers import reverse
from attendance.models import Appointment
from doctors.models import Doctor


class AppointmentValidCreation(TestCase):
    def setUp(self):
        self.appointments = []
        for hour in range(9, 19):
            self.doctor = Doctor.objects.create(forename='one', surname='one', slug='one', area='default')
            self.appointment = Appointment.objects.create(
                            doctor = self.doctor,
                            forename = 'test',
                            surname = 'test',
                            date = datetime.date.today(),
                            time = datetime.time(hour)
                            )
            self.appointments.append(self.appointment)

    def test_creation(self):
        for app in self.appointments:
            self.assertTrue(isinstance(app, Appointment))
        self.assertEqual(Appointment.objects.count(), 10) # should be created only 10 appointments


class AppointmentInvalidCreation(TestCase):
    def test_wrong_time(self):
        wrong_time = datetime.time(8)
        doctor = Doctor.objects.create(forename='one', surname='one', slug='one', area='default')
        appointment = Appointment.objects.create(
                        doctor = doctor,
                        forename = 'test',
                        surname = 'test',
                        date = datetime.date.today(),
                        time = wrong_time
                        )
        self.assertEqual(Appointment.objects.count(), 1)


class AppointmentUniqueCheck(TransactionTestCase):
    def test_unique_fields(self):
        with self.assertRaises(IntegrityError):
            doctor = Doctor.objects.create(forename='one', surname='one', slug='one', area='default')
            for i in range(2):
                appointment = Appointment.objects.create(
                                doctor = doctor,
                                forename = 'test',
                                surname = 'test',
                                date = datetime.date.today(),
                                time = datetime.time(8)
                                )
        self.assertEqual(Appointment.objects.count(), 1)


class ViewsTestCase(TestCase):
    def test_index(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'attendance/index.html')

    def test_appointment_create(self):
        response = self.client.get(reverse('main'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('doctors' in response.context)

    def test_invalid_appointment_form(self):
        # Send junk POST data with invalid choice
        wrong_time = datetime.time(5)
        data = {
        'date': datetime.date.today(),
        'time': wrong_time,
        'forename': 'text',
        'surname': 'text',
        'doctor': 1000
        }
        response = self.client.post(reverse('main'), data)
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response, 'form', 'doctor',
            'Select a valid choice. That choice is not one of the available choices.')
        self.assertFormError(response, 'form', 'time',
            'Select a valid choice. {} is not one of the available choices.'.format(wrong_time))

    def test_valid_appointment_form(self):
        doctor = Doctor.objects.create(forename='one', surname='one', slug='one', area='default')
        data = {
        'date': datetime.date.today(),
        'time': datetime.time(9),
        'forename': 'text',
        'surname': 'text',
        'doctor': doctor.id
        }

        self.assertEqual(Appointment.objects.count(), 0)
        response = self.client.post(reverse('main'), data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], '/schedule/')
        self.assertEqual(Appointment.objects.count(), 1)

    def test_doctor_create(self):
        doctor = Doctor.objects.create(forename='one', surname='one', slug='one', area='default')
        response = self.client.get(reverse('doctor', kwargs={'slug':'one'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('appointments' in response.context)
        self.assertTrue('doctor' in response.context)
    
    def test_doctor_404(self):
        response = self.client.get(reverse('doctor', kwargs={'slug': 'test'}))
        self.assertEqual(response.status_code, 404)



