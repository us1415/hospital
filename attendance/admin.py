from django.contrib import admin
from attendance.models import Appointment

@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('forename', 'surname', 'doctor', 'date', 'time', 'status'),
            'description': "Add the appointment here manually"
        }),
    )
    list_display = ['forename', 'surname', 'doctor', 'date', 'time', 'status']
    list_filter = ['doctor']
    search_fields = ['forename', 'surname']

