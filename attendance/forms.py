from django import forms
from attendance.models import Appointment

class AppointmentForm(forms.ModelForm):
    class Meta:
        model = Appointment
        fields = ['doctor', 'date', 'time', 'forename', 'surname']

    def clean_date(self):
    	"""
    	Check if day is not a holiday
    	"""
    	date = self.cleaned_data['date']
    	if date.isoweekday() > 5:
    		raise forms.ValidationError('Error. It is holiday.')
    	return date