from attendance.models import Appointment
from doctors.models import Doctor
from attendance.forms import AppointmentForm
from django.views.generic import View, TemplateView, DetailView, CreateView
from django.contrib.messages.views import SuccessMessageMixin


class HomePageView(TemplateView):
    template_name = "attendance/index.html"


class AppointmentCreateView(SuccessMessageMixin, CreateView):
    model = Appointment
    template_name =  "attendance/schedule.html"
    form_class = AppointmentForm
    success_url = "/schedule/"
    success_message = "Well done"

    def get_context_data(self, **kwargs):
        context = super(AppointmentCreateView, self).get_context_data(**kwargs)
        context["doctors"] = Doctor.objects.all()
        return context


class DoctorDetailView(DetailView):
    model = Doctor
    context_object_name = "doctor"
    template_name = "attendance/doctor_detail.html"

    def get_context_data(self, **kwargs):
        context = super(DoctorDetailView, self).get_context_data(**kwargs)
        context["appointments"] = self.object.appointments.all()
        return context
