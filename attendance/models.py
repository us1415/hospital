import datetime
from django.db import models
from django.core.exceptions import ValidationError
from doctors.models import Doctor


class Appointment(models.Model):
    class Meta:
        ordering = ['date', 'time']
        unique_together = ('date', 'time')

    def make_times():
        time_tuple = ()
        for i in range(9, 19):
            time_tuple += ((datetime.time(i), datetime.time(i).strftime("%I:%M %p")),)
        return time_tuple

    TIME_CHOICES = make_times()

    AWAITING = 1
    DONE = 2
    CANCELED = 3
    STATUS_CHOICES = (
        (AWAITING,     'Patient is awaiting the appointment'), 
        (DONE,      'Appointment is done'),
        (CANCELED,   'Appointment is canceled'),
    )

    status = models.IntegerField(choices=STATUS_CHOICES, default=AWAITING)
    doctor = models.ForeignKey(Doctor, related_name="appointments")
    date = models.DateField()
    time = models.TimeField(choices=TIME_CHOICES)
    forename = models.CharField(max_length=255)
    surname = models.CharField(max_length=255)

    def clean(self):
        if not self.time in [datetime.time(i) for i in range(9, 19)]:
            raise ValidationError('This time is not allowed')

    def unique_error_message(self, model_class, unique_check):
        if model_class == type(self) and unique_check == ('date', 'time'):
            return 'Already busy. Choose another time period.'
        else:
            return super(Appointment, self).unique_error_message(model_class, unique_check)
    
    def __str__(self):
        return "%s %s"%(self.forename, self.surname) + " " + str("%s %s"%(self.time, self.date))

