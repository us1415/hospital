from django.conf.urls import include, url
from attendance.views import HomePageView, AppointmentCreateView, DoctorDetailView

urlpatterns = [
    url(r'^$', AppointmentCreateView.as_view(), name='main'),
    url(r'^(?P<slug>[\w\-]+)/$', DoctorDetailView.as_view(), name='doctor'),
]
