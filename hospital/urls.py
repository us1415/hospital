from django.conf.urls import include, url
from django.contrib import admin
from attendance.views import HomePageView
from attendance import urls as at_urls
from api import urls as api_urls

urlpatterns = [
    url(r'^$', HomePageView.as_view(), name='index'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^schedule/', include(at_urls)),
    url(r'^api/', include(api_urls))
]
